Jedno z zada� wykonane na przedmiocie Narz�dzia sztucznych inteligencji z 
semestru IV, jego tre�� znajduje si� w pliku tre��.

Jest to w�asnor�czna implementacja sieci BAM (pami�ci asocjacyjnej) wytrenowanej 
do pami�tania par obraz�w. W programie mo�na wybra� jedn� z 3 par i sprawdzi� ile sie� 
pami�ta w zale�no�ci od poziomu szumu. Istnieje tak�e mo�liwo�� wy�wietlenia wykresu 
poprawno�ci od poziomu szumu.   
