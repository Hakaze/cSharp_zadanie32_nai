﻿using NAI_32.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NAI_32
{
    public class Drawer
    {
        public PictureBox pictureArea { get; set; }
        public int[][] data { get; set; }
        public int[] gridSize { get; set; }
        public int[] drawingOffset { get; set; }
        public int[] currentGridSize { get; set; }
        public int cellSize { get; set; }
        public Pen gridLinesPen { get; set; }
        public SolidBrush blackBrush { get; set; }
        public Drawer(int sizeX, int sizeY, PictureBox pictureArea, int[][] data)
        {
            this.data = data;
            this.pictureArea = pictureArea;
            gridSize = new int[2] { sizeY, sizeX };
            drawingOffset = new int[2];
            currentGridSize = new int[2];
            gridLinesPen = new Pen(Color.Gray);
            blackBrush = new SolidBrush(Color.Black);
        }
        
        public void drawGrid(PaintEventArgs e)
        {
            int cellY = pictureArea.Height / gridSize[0];
            int cellX = pictureArea.Width / gridSize[1];
            if (cellY < cellX)
            {
                cellSize = cellY;
            }
            else
            {
                cellSize = cellX;
            }
            currentGridSize[1] = cellSize * gridSize[1];
            currentGridSize[0] = cellSize * gridSize[0];
            drawingOffset[0] = (pictureArea.Height - currentGridSize[0]) / 2;
            drawingOffset[1] = (pictureArea.Width - currentGridSize[1]) / 2;

            for (int i = 0; i <= gridSize[0]; i++)
            {
                e.Graphics.DrawLine(gridLinesPen, drawingOffset[1] , drawingOffset[0] + i * cellSize, drawingOffset[1] + currentGridSize[1], drawingOffset[0] + i * cellSize);
            }
            for (int i = 0; i <= gridSize[1]; i++)
            {
                e.Graphics.DrawLine(gridLinesPen, i * cellSize + drawingOffset[1], drawingOffset[0], i * cellSize + drawingOffset[1], drawingOffset[0] + currentGridSize[0]);
            }
            for (int i = 0; i < data.Length; i++)
            {
                for (int j = 0; j < data[i].Length; j++)
                {
                    if (data[i][j] == 1)
                    {
                        e.Graphics.FillRectangle(blackBrush, j * cellSize + drawingOffset[1], i * cellSize + drawingOffset[0], cellSize, cellSize);

                }
            }
            }
        }

        
    }
}
