﻿using NAI_32.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NAI_32
{
    public partial class ChartForm : Form
    {
        public double[] results { get; set; }
        public ChartForm(double[] results)
        {
            InitializeComponent();
            this.results = results;
            initializechart();
        }
        private void initializechart()
        {
            mainChart.Series.Clear();
            mainChart.Series.Add("Poprawność % odpowiedzi względem szumu");
            mainChart.ChartAreas[0].AxisX.Interval = 1;
            mainChart.Series[0].BorderWidth = 5;
            mainChart.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            for(int i=0; i<101; i++)
            {
                mainChart.Series[0].Points.AddXY(i, results[i]);
            }
        }
    }
}
