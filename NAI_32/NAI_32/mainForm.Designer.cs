﻿namespace NAI_32
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.networkGroupBox = new System.Windows.Forms.GroupBox();
            this.validateChartButton = new System.Windows.Forms.Button();
            this.checkButton = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.validateTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.weightsDataGridView = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.noiseTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.networksListBox = new System.Windows.Forms.ListBox();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.visualizationsSplitContainer = new System.Windows.Forms.SplitContainer();
            this.inputDataGroupBox = new System.Windows.Forms.GroupBox();
            this.outputSplitContainer = new System.Windows.Forms.SplitContainer();
            this.desiredOutputGroupBox = new System.Windows.Forms.GroupBox();
            this.realOutputGroupBox = new System.Windows.Forms.GroupBox();
            this.inputPictureBox = new System.Windows.Forms.PictureBox();
            this.desireOutputPictureBox = new System.Windows.Forms.PictureBox();
            this.realOutputPictureBox = new System.Windows.Forms.PictureBox();
            this.networkGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.weightsDataGridView)).BeginInit();
            this.mainPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.visualizationsSplitContainer)).BeginInit();
            this.visualizationsSplitContainer.Panel1.SuspendLayout();
            this.visualizationsSplitContainer.Panel2.SuspendLayout();
            this.visualizationsSplitContainer.SuspendLayout();
            this.inputDataGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.outputSplitContainer)).BeginInit();
            this.outputSplitContainer.Panel1.SuspendLayout();
            this.outputSplitContainer.Panel2.SuspendLayout();
            this.outputSplitContainer.SuspendLayout();
            this.desiredOutputGroupBox.SuspendLayout();
            this.realOutputGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.inputPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.desireOutputPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.realOutputPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // networkGroupBox
            // 
            this.networkGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.networkGroupBox.Controls.Add(this.validateChartButton);
            this.networkGroupBox.Controls.Add(this.checkButton);
            this.networkGroupBox.Controls.Add(this.label8);
            this.networkGroupBox.Controls.Add(this.label7);
            this.networkGroupBox.Controls.Add(this.label6);
            this.networkGroupBox.Controls.Add(this.label5);
            this.networkGroupBox.Controls.Add(this.validateTextBox);
            this.networkGroupBox.Controls.Add(this.label4);
            this.networkGroupBox.Controls.Add(this.weightsDataGridView);
            this.networkGroupBox.Controls.Add(this.label2);
            this.networkGroupBox.Controls.Add(this.noiseTextBox);
            this.networkGroupBox.Controls.Add(this.label3);
            this.networkGroupBox.Controls.Add(this.label1);
            this.networkGroupBox.Controls.Add(this.networksListBox);
            this.networkGroupBox.Location = new System.Drawing.Point(12, 12);
            this.networkGroupBox.Name = "networkGroupBox";
            this.networkGroupBox.Size = new System.Drawing.Size(229, 668);
            this.networkGroupBox.TabIndex = 0;
            this.networkGroupBox.TabStop = false;
            this.networkGroupBox.Text = "Panel kontrolny sieci";
            // 
            // validateChartButton
            // 
            this.validateChartButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.validateChartButton.Location = new System.Drawing.Point(9, 639);
            this.validateChartButton.Name = "validateChartButton";
            this.validateChartButton.Size = new System.Drawing.Size(133, 23);
            this.validateChartButton.TabIndex = 9;
            this.validateChartButton.Text = "Wykres poprawności";
            this.validateChartButton.UseVisualStyleBackColor = true;
            this.validateChartButton.Click += new System.EventHandler(this.validateChartButton_Click);
            // 
            // checkButton
            // 
            this.checkButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkButton.Location = new System.Drawing.Point(148, 639);
            this.checkButton.Name = "checkButton";
            this.checkButton.Size = new System.Drawing.Size(75, 23);
            this.checkButton.TabIndex = 8;
            this.checkButton.Text = "Sprawdź";
            this.checkButton.UseVisualStyleBackColor = true;
            this.checkButton.Click += new System.EventHandler(this.checkButton_Click);
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 560);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(187, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Liczba neuronów drugiej warstwy: 280";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 537);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(199, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Liczba neuronów pierwszej warstwy: 320";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(162, 616);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(15, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "%";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(162, 590);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(15, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "%";
            // 
            // validateTextBox
            // 
            this.validateTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.validateTextBox.Location = new System.Drawing.Point(85, 613);
            this.validateTextBox.Name = "validateTextBox";
            this.validateTextBox.Size = new System.Drawing.Size(71, 20);
            this.validateTextBox.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 616);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Poprawność";
            // 
            // weightsDataGridView
            // 
            this.weightsDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.weightsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.weightsDataGridView.Location = new System.Drawing.Point(9, 156);
            this.weightsDataGridView.Name = "weightsDataGridView";
            this.weightsDataGridView.Size = new System.Drawing.Size(214, 378);
            this.weightsDataGridView.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 140);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Wagi";
            // 
            // noiseTextBox
            // 
            this.noiseTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.noiseTextBox.Location = new System.Drawing.Point(85, 587);
            this.noiseTextBox.Name = "noiseTextBox";
            this.noiseTextBox.Size = new System.Drawing.Size(71, 20);
            this.noiseTextBox.TabIndex = 1;
            this.noiseTextBox.Text = "0";
            this.noiseTextBox.TextChanged += new System.EventHandler(this.noiseTextBox_TextChanged);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 590);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Poziom szumu";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Lista sieci";
            // 
            // networksListBox
            // 
            this.networksListBox.FormattingEnabled = true;
            this.networksListBox.Location = new System.Drawing.Point(6, 42);
            this.networksListBox.Name = "networksListBox";
            this.networksListBox.Size = new System.Drawing.Size(217, 95);
            this.networksListBox.TabIndex = 0;
            this.networksListBox.SelectedIndexChanged += new System.EventHandler(this.networksListBox_SelectedIndexChanged);
            // 
            // mainPanel
            // 
            this.mainPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainPanel.Controls.Add(this.visualizationsSplitContainer);
            this.mainPanel.Location = new System.Drawing.Point(247, 12);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(1047, 668);
            this.mainPanel.TabIndex = 1;
            // 
            // visualizationsSplitContainer
            // 
            this.visualizationsSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.visualizationsSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.visualizationsSplitContainer.Name = "visualizationsSplitContainer";
            // 
            // visualizationsSplitContainer.Panel1
            // 
            this.visualizationsSplitContainer.Panel1.Controls.Add(this.inputDataGroupBox);
            // 
            // visualizationsSplitContainer.Panel2
            // 
            this.visualizationsSplitContainer.Panel2.Controls.Add(this.outputSplitContainer);
            this.visualizationsSplitContainer.Size = new System.Drawing.Size(1047, 668);
            this.visualizationsSplitContainer.SplitterDistance = 349;
            this.visualizationsSplitContainer.TabIndex = 1;
            // 
            // inputDataGroupBox
            // 
            this.inputDataGroupBox.Controls.Add(this.inputPictureBox);
            this.inputDataGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inputDataGroupBox.Location = new System.Drawing.Point(0, 0);
            this.inputDataGroupBox.Name = "inputDataGroupBox";
            this.inputDataGroupBox.Size = new System.Drawing.Size(349, 668);
            this.inputDataGroupBox.TabIndex = 0;
            this.inputDataGroupBox.TabStop = false;
            this.inputDataGroupBox.Text = "Obraz wejściowy";
            // 
            // outputSplitContainer
            // 
            this.outputSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.outputSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.outputSplitContainer.Name = "outputSplitContainer";
            this.outputSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // outputSplitContainer.Panel1
            // 
            this.outputSplitContainer.Panel1.Controls.Add(this.desiredOutputGroupBox);
            // 
            // outputSplitContainer.Panel2
            // 
            this.outputSplitContainer.Panel2.Controls.Add(this.realOutputGroupBox);
            this.outputSplitContainer.Size = new System.Drawing.Size(694, 668);
            this.outputSplitContainer.SplitterDistance = 340;
            this.outputSplitContainer.TabIndex = 0;
            // 
            // desiredOutputGroupBox
            // 
            this.desiredOutputGroupBox.Controls.Add(this.desireOutputPictureBox);
            this.desiredOutputGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.desiredOutputGroupBox.Location = new System.Drawing.Point(0, 0);
            this.desiredOutputGroupBox.Name = "desiredOutputGroupBox";
            this.desiredOutputGroupBox.Size = new System.Drawing.Size(694, 340);
            this.desiredOutputGroupBox.TabIndex = 0;
            this.desiredOutputGroupBox.TabStop = false;
            this.desiredOutputGroupBox.Text = "Obraz oczekiwany";
            // 
            // realOutputGroupBox
            // 
            this.realOutputGroupBox.Controls.Add(this.realOutputPictureBox);
            this.realOutputGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.realOutputGroupBox.Location = new System.Drawing.Point(0, 0);
            this.realOutputGroupBox.Name = "realOutputGroupBox";
            this.realOutputGroupBox.Size = new System.Drawing.Size(694, 324);
            this.realOutputGroupBox.TabIndex = 0;
            this.realOutputGroupBox.TabStop = false;
            this.realOutputGroupBox.Text = "Obraz uzyskany";
            // 
            // inputPictureBox
            // 
            this.inputPictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inputPictureBox.Location = new System.Drawing.Point(3, 16);
            this.inputPictureBox.Name = "inputPictureBox";
            this.inputPictureBox.Size = new System.Drawing.Size(343, 649);
            this.inputPictureBox.TabIndex = 0;
            this.inputPictureBox.TabStop = false;
            this.inputPictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.inputPictureBox_Paint);
            // 
            // desireOutputPictureBox
            // 
            this.desireOutputPictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.desireOutputPictureBox.Location = new System.Drawing.Point(3, 16);
            this.desireOutputPictureBox.Name = "desireOutputPictureBox";
            this.desireOutputPictureBox.Size = new System.Drawing.Size(688, 321);
            this.desireOutputPictureBox.TabIndex = 0;
            this.desireOutputPictureBox.TabStop = false;
            this.desireOutputPictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.desireOutputPictureBox_Paint);
            // 
            // realOutputPictureBox
            // 
            this.realOutputPictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.realOutputPictureBox.Location = new System.Drawing.Point(3, 16);
            this.realOutputPictureBox.Name = "realOutputPictureBox";
            this.realOutputPictureBox.Size = new System.Drawing.Size(688, 305);
            this.realOutputPictureBox.TabIndex = 0;
            this.realOutputPictureBox.TabStop = false;
            this.realOutputPictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.realOutputPictureBox_Paint);
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1306, 692);
            this.Controls.Add(this.mainPanel);
            this.Controls.Add(this.networkGroupBox);
            this.Name = "mainForm";
            this.Text = "BAM Network";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.networkGroupBox.ResumeLayout(false);
            this.networkGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.weightsDataGridView)).EndInit();
            this.mainPanel.ResumeLayout(false);
            this.visualizationsSplitContainer.Panel1.ResumeLayout(false);
            this.visualizationsSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.visualizationsSplitContainer)).EndInit();
            this.visualizationsSplitContainer.ResumeLayout(false);
            this.inputDataGroupBox.ResumeLayout(false);
            this.outputSplitContainer.Panel1.ResumeLayout(false);
            this.outputSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.outputSplitContainer)).EndInit();
            this.outputSplitContainer.ResumeLayout(false);
            this.desiredOutputGroupBox.ResumeLayout(false);
            this.realOutputGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.inputPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.desireOutputPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.realOutputPictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox networkGroupBox;
        private System.Windows.Forms.TextBox validateTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView weightsDataGridView;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox noiseTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox networksListBox;
        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.SplitContainer visualizationsSplitContainer;
        private System.Windows.Forms.GroupBox inputDataGroupBox;
        private System.Windows.Forms.SplitContainer outputSplitContainer;
        private System.Windows.Forms.GroupBox desiredOutputGroupBox;
        private System.Windows.Forms.GroupBox realOutputGroupBox;
        private System.Windows.Forms.Button validateChartButton;
        private System.Windows.Forms.Button checkButton;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.PictureBox inputPictureBox;
        public System.Windows.Forms.PictureBox desireOutputPictureBox;
        public System.Windows.Forms.PictureBox realOutputPictureBox;
    }
}

