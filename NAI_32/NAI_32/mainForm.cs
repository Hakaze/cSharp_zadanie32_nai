﻿using NAI_32.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NAI_32
{
    public partial class mainForm : Form
    {
        public Data data { get; set; }
        public NetworkBAM network { get; set; }
        public Drawer inputDrawer { get; set; }
        public Drawer realOutputDrawer { get; set; }
        public Drawer desireOutputDrawer { get; set; }
        bool readyToDraw;

        bool LEARNING_MODE;
        public mainForm()
        {
            InitializeComponent();
            Initialize();
        }
        private void Initialize()
        {
            LEARNING_MODE = false;
            readyToDraw = false;
            LoadNetworks();
            
            networksListBox.SelectedIndex = 0;
            inputDrawer = new Drawer(16, 20, inputPictureBox, data.inputVector);
            realOutputDrawer = new Drawer(35, 8, realOutputPictureBox, data.realOutputVector);
            desireOutputDrawer = new Drawer(35, 8, desireOutputPictureBox, data.outputVector);
            readyToDraw = true;
            invalidateDrawAreas();
            if (!LEARNING_MODE)
            {
                LoadNetwork();
            }
            else
            {
                network = new NetworkBAM(320, 280);
            }

        }
        private void LoadNetworks()
        {
            foreach (string f in Directory.GetFiles("Data"))
            {
                networksListBox.Items.Add(f);
            }
        }
        
        private void networksListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadData();
            
            if (readyToDraw)
            {
                invalidateDrawAreas();
            }

        }
        private void LoadData()
        {
            try
            {
                /*
                String dataName = networksListBox.SelectedItem.ToString();
                String path="";
                dataName = dataName.Substring(9, dataName.Length - 20);
                //MessageBox.Show(dataName);
                foreach (string f in Directory.GetFiles("Data"))
                {
                    //MessageBox.Show("Sprawdzanie " + f);
                    if (f.Contains(dataName))
                    {
                        path = f;
                    }
                }
                */
                using (StreamReader sr = new StreamReader(networksListBox.SelectedItem.ToString()))
                {
                    String line = sr.ReadLine();
                    // int inputWidth, int inputHeigth, int outputWidth, int outputHeigth
                    String[] networkParameters = line.Split(' ');

                    data = new Data(Int32.Parse(networkParameters[0]), Int32.Parse(networkParameters[1]), Int32.Parse(networkParameters[2]), Int32.Parse(networkParameters[3]));
                    for (int i = 0; i < data.inputVector.Length; i++)
                    {
                        line = sr.ReadLine();
                        char[] lineChar = line.ToCharArray();
                        for (int j = 0; j < lineChar.Length; j++)
                        {
                            data.inputVector[i][j] = Int32.Parse(lineChar[j] + "");
                            data.changedInputVector[i][j] = data.inputVector[i][j];
                        }
                    }
                    for (int i = 0; i < data.outputVector.Length; i++)
                    {
                        line = sr.ReadLine();
                        char[] lineChar = line.ToCharArray();
                        for (int j = 0; j < lineChar.Length; j++)
                        {
                            data.outputVector[i][j] = Int32.Parse(lineChar[j] + "");
                        }
                    }
                    //data.changedInputVector = data.inputVector;
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show("The file could not be read: "+exc.Message);
            }
        }
        private void LoadNetwork()
        {
            try
            {
                using (StreamReader sr = new StreamReader("Networks/n1.txt"))
                {
                    String line = sr.ReadLine();
                    // firstLayerNeuronsAmount, secondLayerNeuronsAmount
                    String[] networkParameters = line.Split(' ');
                    network = new NetworkBAM( Int32.Parse(networkParameters[0]), Int32.Parse(networkParameters[1]));
                    String[] weights;
                    for (int i = 0; i < network.weights.Length; i++)
                    {
                        line = sr.ReadLine();
                        weights = line.Split(';');
                        for (int j = 0; j < weights.Length; j++)
                        {
                            network.weights[i][j] = Int32.Parse(weights[j]);
                        }
                    }


                }
            }
            catch (Exception exc)
            {
                MessageBox.Show("NETWORK: The file could not be read: " + exc.Message);
            }
        }

        private void inputPictureBox_Paint(object sender, PaintEventArgs e)
        {
            if(readyToDraw)
                inputDrawer.drawGrid(e);
        }

        private void desireOutputPictureBox_Paint(object sender, PaintEventArgs e)
        {
            if (readyToDraw)
                desireOutputDrawer.drawGrid(e);
        }

        private void realOutputPictureBox_Paint(object sender, PaintEventArgs e)
        {
            if (readyToDraw)
                realOutputDrawer.drawGrid(e);
        }
        private void invalidateDrawAreas()
        {
            inputDrawer.data = data.inputVector;
            realOutputDrawer.data = data.realOutputVector;
            desireOutputDrawer.data = data.outputVector;

            inputPictureBox.Invalidate();
            realOutputPictureBox.Invalidate();
            desireOutputPictureBox.Invalidate();
        }

        private void noiseTextBox_TextChanged(object sender, EventArgs e)
        {
            
            if (noiseTextBox.Text == "")
            {
                MessageBox.Show("Wartość procentowa szumu nie może być pusta");
                return;
            }
            int prob = 0;
            if(Int32.TryParse(noiseTextBox.Text, out prob))
            {
                data.generateNoise(prob);
                inputDrawer.data = data.changedInputVector;
                inputPictureBox.Invalidate();
                if (!LEARNING_MODE)
                {
                    // obliczyc, sprawdzic poprawnosc
                }
            }
            else
            {
                MessageBox.Show("Wartość procentowa szumu musi być liczbą całkowitą");
            }
        }

        private void validateChartButton_Click(object sender, EventArgs e)
        {

            if (LEARNING_MODE)
            {
                network.learn(data);
            }
            else
            {
                double[] result = new double[101];
                for(int i =0; i<101; i++)
                {
                    data.generateNoise(i);
                    data.realOutputVector = network.validate(data);
                    result[i] = data.verifyPercent();
                }
                ChartForm chF = new ChartForm(result);
                chF.ShowDialog();
            }
            

        }

        private void checkButton_Click(object sender, EventArgs e)
        {

            if (LEARNING_MODE)
            {
                network.saveNetwork();
            }
            else
            {
                data.realOutputVector = network.validate(data);
                realOutputDrawer.data = data.realOutputVector;
                validateTextBox.Text = data.verifyPercent() + "";
                realOutputPictureBox.Invalidate();
            }
        }
    }
}
