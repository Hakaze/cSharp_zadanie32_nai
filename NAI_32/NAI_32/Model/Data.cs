﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NAI_32.Model
{
    public class Data
    {
        public int[][] inputVector { get; set; }
        public int[][] changedInputVector { get; set; }
        public int[][] outputVector { get; set; }
        public int[][] realOutputVector { get; set; }
        public Random random { get; set; }
        public Data(int inputWidth, int inputHeigth, int outputWidth, int outputHeigth)
        {
            random = new Random();
            inputVector = new int[inputHeigth][];
            changedInputVector = new int[inputHeigth][];
            for (int i=0; i< inputVector.Length; i++)
            {
                inputVector[i] = new int[inputWidth];
                changedInputVector[i] = new int[inputWidth];
            }
            outputVector = new int[outputHeigth][];
            realOutputVector= new int[outputHeigth][];
            for (int i = 0; i < outputVector.Length; i++)
            {
                outputVector[i] = new int[outputWidth];
                realOutputVector[i] = new int[outputWidth];
            }

        }
        public void generateNoise(int prob)
        {

            for (int i = 0; i < inputVector.Length; i++)
            {
                for (int j = 0; j < inputVector[i].Length; j++)
                {
                    int rand = random.Next(0, 101);
                    if (rand < prob)
                    {
                        changedInputVector[i][j] = random.Next(0, 2);
                    }
                    else
                    {
                        changedInputVector[i][j] = inputVector[i][j];
                    }

                }
            }
        }
        public double verifyPercent()
        {
            double sum = 0;
            for(int i=0; i<realOutputVector.Length; i++)
            {
                for(int j=0; j< realOutputVector[i].Length; j++)
                {
                    
                    if (realOutputVector[i][j] == outputVector[i][j])
                    {
                        sum++;
                    }
                }
            }
            return sum / (realOutputVector.Length * realOutputVector[0].Length) * 100;
            
        }

    }

}
