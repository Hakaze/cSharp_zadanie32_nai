﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace NAI_32.Model
{
    public class NetworkBAM
    {
        public int[][] weights { get; set; }
        public int[][] weightsTransponed { get; set; }
        public int[][] lastAnwserFirstLayer { get; set; }
        public int[][] lastAnwserSecondLayer { get; set; }

        public NetworkBAM( int firstLayerSize, int secondLayerSize)
        {
            weights = new int[firstLayerSize][];
            for (int i=0; i< weights.Length; i++)
            {
                weights[i] = new int[secondLayerSize];
            }
        }
        public void learn(Data data)
        {
            int[] inputTemp = new int[data.inputVector.Length * data.inputVector[0].Length];
            int counter = 0;
            for (int i = 0; i < data.inputVector.Length; i++)
            {
                for (int j = 0; j < data.inputVector[0].Length; j++)
                {
                    if (data.inputVector[i][j] == 0)
                    {
                        inputTemp[counter] = -1;
                    }
                    else
                    {
                        inputTemp[counter] = data.inputVector[i][j];
                    }
                    
                    counter++;
                }

            }
            int[] outputTemp = new int[data.outputVector.Length * data.outputVector[0].Length];
            counter = 0;
            for (int i = 0; i < data.outputVector.Length; i++)
            {
                for (int j = 0; j < data.outputVector[0].Length; j++)
                {
                    if (data.outputVector[i][j] == 0)
                    {
                        outputTemp[counter] = -1;
                    }
                    else
                    {
                        outputTemp[counter] = data.outputVector[i][j];
                    }
                    
                    counter++;
                }

            }
            for (int i = 0; i < weights.Length; i++)
            {
                for(int j=0; j< weights[i].Length; j++)
                {
                    weights[i][j] = weights[i][j] + inputTemp[i] * outputTemp[j];
                }
               
            }
        }
        public void saveNetwork()
        {
            System.IO.StreamWriter file = new System.IO.StreamWriter("Networks/n1.txt");
            for (int i = 0; i < weights.Length; i++)
            {
                for (int j = 0; j < weights[i].Length-1; j++)
                {
                    file.Write(weights[i][j]+";");
                }
                file.Write(weights[i][weights[i].Length - 1]);
                file.Write("\r\n");

            }
            file.Close();
        }
        public int[][] validate(Data data)
        {
            bool processing = true;
            weightsTransponed = transponeMatrix(weights);
            lastAnwserFirstLayer = covertToOneRowMatrix(data.changedInputVector);
            lastAnwserSecondLayer = matrixMultiply(lastAnwserFirstLayer, weights);

            while (processing)
            {
                processing = false;
                int[][] resultFirst = matrixMultiply(lastAnwserSecondLayer, weightsTransponed);
                int[][] resultSecond = matrixMultiply(resultFirst, weights);
                for(int i=0; i< resultFirst.Length; i++)
                {
                    for(int j=0; j< resultFirst[i].Length; j++)
                    {
                        if(lastAnwserFirstLayer[i][j]!= resultFirst[i][j])
                        {
                            processing = true;
                            
                        }
                    }
                }
                for (int i = 0; i < resultSecond.Length; i++)
                {
                    for (int j = 0; j < resultSecond[i].Length; j++)
                    {
                        if (lastAnwserSecondLayer[i][j] != resultSecond[i][j])
                        {
                            processing = true;
                            
                        }
                    }
                }
                lastAnwserFirstLayer = resultFirst;
                lastAnwserSecondLayer = resultSecond;
            }

            /*
            int[][] result = matrixMultiply(covertToOneRowMatrix(data.changedInputVector), weights);
            int counter = 0;
            for(int i=0; i< data.outputVector.Length; i++)
            {
                for(int j=0; j< data.outputVector[i].Length; j++)
                {
                    //data.realOutputVector[i][j] = oneColResult[counter];
                    if (result[0][counter] < 0)
                    {
                        data.realOutputVector[i][j] = 0;
                    }
                    else if(result[0][counter]>0)
                    {
                        data.realOutputVector[i][j] = 1;
                    }
                    else
                    {
                        //data.realOutputVector[i][j]
                    }
                    //data.realOutputVector[i][j] = result[0][counter];
                    counter++;
                }
            }
            */
            int counter = 0;
            for(int i=0; i< data.realOutputVector.Length; i++)
            {
                for(int j=0; j< data.realOutputVector[i].Length; j++)
                {
                    if (lastAnwserSecondLayer[0][counter] == -1)
                    {
                        lastAnwserSecondLayer[0][counter] = 0;
                    }
                    data.realOutputVector[i][j] = lastAnwserSecondLayer[0][counter];
                    counter++;
                }
            }
            return data.realOutputVector; //lastAnwserSecondLayer; //
        }
        public int[][] matrixMultiply(int[][] matrixA, int[][] matrixB)
        {
            int[][] result = new int[matrixA.Length][];
            for (int i = 0; i < result.Length; i++)
            {
                result[i] = new int[matrixB[0].Length];
            }
            for(int i=0; i<result.Length; i++)
            {
                for (int j = 0; j < result[i].Length; j++)
                {
                    int sum = 0;
                    for (int k = 0; k < matrixA[0].Length; k++)
                    {
                        sum = sum + matrixA[i][k] * matrixB[k][j];
                    }
                    if (sum > 0)
                    {
                        result[i][j] = 1;
                    }
                    else
                    {
                        result[i][j] = -1;
                    }
                    //result[i][j] = sum;
                }
            }
            return result;
        }
        public int[][] transponeMatrix(int[][] matrix)
        {
            int[][] result = new int[matrix[0].Length][];
            for(int i=0; i< result.Length; i++)
            {
                result[i] = new int[matrix.Length];
                for(int j=0; j< result[i].Length; j++)
                {
                    result[i][j] = matrix[j][i];
                }
            }
            return result;
        }
        public int[][] covertToOneRowMatrix(int[][] matrix)
        {
            int[][] result = new int[1][];
            result[0] = new int[matrix.Length * matrix[0].Length];
            int counter = 0;
            for(int i=0; i<matrix.Length; i++)
            {
                for(int j=0; j<matrix[i].Length; j++)
                {
                    result[0][counter] = matrix[i][j];
                    counter++;
                }
            }
            return result;
        }

    }
}
